// Medidor de temperatura e luminosidade controla LED
float val_lum;
int luminosidade;
byte LDRpin=A0; 
byte LED = 13;

void setup() {
  Serial.begin(9600);
}

void loop() {

 val_lum = analogRead(LDRpin);
 luminosidade = val_lum*0.0977;

 Serial.write("Luminosidade: ");
 Serial.println(luminosidade);  

 
 if (luminosidade < 50) 
 {
  digitalWrite(LED, HIGH);
 }

else if (luminosidade > 50)
  {
  digitalWrite(LED,LOW);
 }
  delay(1000); // os dados serao atualizados a cada 1s (1000ms)
  }

