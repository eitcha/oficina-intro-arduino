# Oficina de Introdução ao Arduino

Neste repositório é possível encontrar todo o material usado na oficina de introdução ao Arduino da EITCHA (eitcha.org). Este material foi baseado no trabalho do Centro Acadêmico de Tecnologia (cta.if.ufrgs.br) da UFRGS, material original encontram-se em http://cta.if.ufrgs.br/projects/suporte-cta/wiki/Portf%C3%B3lio_de_Oficinas#Introdu%C3%A7%C3%A3o-ao-Arduino .

Resumo da atividade:

O Arduino é uma plataforma de prototipagem eletrônica de hardware livre, projetada com um microcontrolador Atmel AVR de placa única, com suporte de entrada/saída embutido. Pode ser usado para o desenvolvimento de objetos interativos independentes, ou ainda para ser conectado a um computador hospedeiro1. O Arduino é base de diversos projetos do CTA, como as Estações Meteorológicas Modulares, a Fresadora PCI João de Barro, a Estação de Espectrometria, entre outros.

Essa oficina consiste de uma breve descrição sobre o Arduino seguida de montagem de projetos que utilizam a placa. Antes de cada projeto, são introduzidos os conceitos básicos de eletrônica e programação em C necessários para o entendimento do projeto.

Tópicos abordados:

    Introdução
        Conceitos básicos de eletricidade
        Plataforma Arduino
        Prototipagem em protoboard

    Projetos
        Conceitos básicos de eletrônica e programação em C
        Montagem de projetos

    Lotação máxima: 30 pessoas

Materiais necessários:

Por dupla de participantes:

    1 computador com a IDE do Arduino instalada;
    1 placa Arduino;
    1 cabo USB Arduino;
    1 protoboard;
    1 potenciômetro;
    1 buzzer;
    1 LDR
    1 LED;
    2 resistor de 100 ohms;
    1 resistor 10k ohms;
    5 jumpers pequenos;
    4 jumpers grandes
    1 banana.

Tempo de duração aproximado: de 3 a 4 horas.
